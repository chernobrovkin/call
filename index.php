<?php
require_once $_SERVER['DOCUMENT_ROOT'] . '/lib/autoload.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/vendor/autoload.php';

\Ibrush\Expo19\Application::getInstance()->run();