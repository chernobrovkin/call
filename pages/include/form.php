<section class="visit-form__section visit-form__section_contacts is-active" data-visit-slide="contacts">
    <div class="contact-us-form">
        <h2 class="contact-us-form__title">О&nbsp;Вас</h2>

        <form action="/" class="form__form js-form" method="POST">
            <div class="form__row">
                <div class="form__field is-floating-label">
                    <label class="form__label" for="company">Ваша компания</label>
                    <input class="form__input form__input_name" type="text" name="COMPANY_NAME">
                </div>
            </div>

            <div class="form__row">
                <div class="form__field is-floating-label">
                    <label class="form__label" for="name">Ваше имя</label>
                    <input class="form__input form__input_name" type="text" name="NAME">
                </div>
            </div>

            <div class="form__row">
                <div class="form__field is-floating-label">
                    <label class="form__label" for="surname">Ваше фамилия</label>
                    <input class="form__input form__input_name" type="text" name="SECOND_NAME">
                </div>
            </div>

            <div class="form__row">
                <div class="form__field is-floating-label">
                    <label class="form__label" for="post">Должность</label>
                    <input class="form__input form__input_name" type="text" name="POSITION">
                </div>
            </div>

            <div class="form__row">
                <div class="form__field is-floating-label">
                    <label class="form__label" for="phone">Телефон</label>
                    <input class="form__input form__input_phone is-floating-label _mask-phone" type="tel" name="PHONE" placeholder="" pattern="([\+][0-9][ ][\(][0-9]{3}[\)][ ][0-9]{3}[-][0-9]{2}[-][0-9]{2})" value="<?= \Ibrush\Expo19\Helper::getCurrentPhone(false);?>">
                </div>
            </div>

            <div class="form__row">
                <div class="form__field is-floating-label">
                    <label class="form__label" for="email">Электронная почта</label>
                    <input id="email" class="form__input form__input_email" type="text" name="EMAIL" placeholder="">
                </div>
            </div>

            <div class="form__row form__row_submit">
                <button type="submit" class="form__button">Сохранить</button>
            </div>
        </form>
    </div>
</section>