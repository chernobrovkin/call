<section class="visit-form__section visit-form__section_intro is-active js-parallax" data-visit-slide="intro">
    <div class="visit-form__points js-parallax-layer" data-parallax-deep="3000">
        <img src="/assets/img/points/point-1.1.svg" alt="" class="visit-form__point visit-form__point_1">
        <img src="/assets/img/points/point-1.2.svg" alt="" class="visit-form__point visit-form__point_2">
        <img src="/assets/img/points/point-1.3.svg" alt="" class="visit-form__point visit-form__point_3">
        <img src="/assets/img/points/point-1.4.svg" alt="" class="visit-form__point visit-form__point_4">
        <img src="/assets/img/points/point-1.5.svg" alt="" class="visit-form__point visit-form__point_5">
        <img src="/assets/img/points/point-1.6.svg" alt="" class="visit-form__point visit-form__point_6">
        <img src="/assets/img/points/point-1.7.svg" alt="" class="visit-form__point visit-form__point_7">
        <img src="/assets/img/points/point-1.8.svg" alt="" class="visit-form__point visit-form__point_8">
    </div>

    <div class="visit-form__intro">
        <h2 class="visit-form__title visit-form__title_intro"><span class="nowrap">Здесь бесплатно</span> <span
                    class="visit-form__title-highlight">наливают</span></h2>

        <div class="visit-form__contacts">
            <p>
                За&nbsp;визитку, звонок по&nbsp;номеру или письмо на&nbsp;почту :)
            </p>

            <ul class="contacts-list">
                <li class="contacts-list__item">
                    <span class="visit-form__highlight">+7&nbsp;495&nbsp;540-47-68</span>
                </li>
                <li class="contacts-list__item">
                    <a href="mailto:coffee@ibrush.ru" class="visit-form__link visit-form__highlight">coffee@ibrush.ru
                    </a>
                </li>
            </ul>
        </div>
    </div>

    <div class="visit-form__intro-img-wrap js-parallax-layer" data-parallax-deep="1000">
        <img src="/assets/img/coffee.png" alt="Изображение чашки кофе" class="visit-form__intro-img">
    </div>
</section>