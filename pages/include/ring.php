
<section class="visit-form__section visit-form__section_calling is-active" data-visit-slide="calling">
    <div class="calling">
        <div class="calling__inner">
            <p class="calling__title">Входящий звонок</p>
            <div class="calling__phone">
                <span class="calling__phone-value"><?= \Ibrush\Expo19\Helper::getCurrentPhone();?></span>
            </div>
        </div>

    </div>
</section>