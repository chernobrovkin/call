<section class="visit-form__section visit-form__section_finish js-parallax is-active" data-visit-slide="finish">
    <h2 class="visit-form__title">Угощайтесь! <img src="/assets/img/coffee-enjoy.png" alt="Кофе" class="visit-form__title-img"></h2>

    <div class="visit-form__points js-parallax-layer" data-parallax-deep="3000">
        <img src="/assets/img/points/point-1.1.svg" alt="" class="visit-form__point visit-form__point_9">
        <img src="/assets/img/points/point-1.2.svg" alt="" class="visit-form__point visit-form__point_10">
        <img src="/assets/img/points/point-1.3.svg" alt="" class="visit-form__point visit-form__point_11">
        <img src="/assets/img/points/point-1.4.svg" alt="" class="visit-form__point visit-form__point_12">
        <img src="/assets/img/points/point-1.5.svg" alt="" class="visit-form__point visit-form__point_13">
        <img src="/assets/img/points/point-1.6.svg" alt="" class="visit-form__point visit-form__point_14">
        <img src="/assets/img/points/point-1.7.svg" alt="" class="visit-form__point visit-form__point_15">
        <img src="/assets/img/points/point-1.8.svg" alt="" class="visit-form__point visit-form__point_16">
    </div>
</section>