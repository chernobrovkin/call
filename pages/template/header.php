<!DOCTYPE html>
<html lang="ru">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="format-detection" content="telephone=no">

    <title>Развитие и поддержка</title>

    <link rel="stylesheet" href="/assets/css/main.css">
    <link rel="stylesheet" href="/assets/css/custom.css">
    <link rel="icon" type="image/png" sizes="16x16" href="/assets/favicon/favicon.ico">
    <link rel="manifest" href="/assets/favicon/manifest.json">
    <link rel="mask-icon" href="/assets/favicon/safari-pinned-tab.svg" color="#5bbad5">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,700,800&amp;subset=cyrillic" rel="stylesheet">

    <meta name="theme-color" content="#ffffff">
    <meta property="og:image" content="http://www.ibrush.ru/share.png">
</head>

<body>
<div class="site-wrapper" data-state="<?= \Ibrush\Expo19\Application::getInstance()->getState()?>">
    <div class="js-reset reset-button"></div>
    <!-- CONTENT -->
    <main class="site-content">


        <!-- VISIT FORM -->
        <div class="visit-form">