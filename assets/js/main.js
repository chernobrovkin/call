'use strict';

svg4everybody();

$(function () {

    if ($('.visit-form__section_finish.is-active').length) {
        setTimeout(function () {
            $.post({
                url: document.location.pathname,
                data: {
                    method: 'changeState'
                },
                success: function() {
                    document.location.reload(true);
                }
            });
        }, 5000);
    }

    setInterval(function () {
        $.post({
            url: document.location.pathname,
            data: {
                method: 'getState'
            },
            success: function(res) {
                if ($('.site-wrapper').attr('data-state') !== res) {
                    document.location.reload(true);
                }
            }
        });
    }, 1000);

    var $resetButton = $('.js-reset');
    $resetButton.click(function () {
        $.post({
            url: document.location.pathname,
            data: {
                method: 'resetState'
            },
            success: function() {
                document.location.reload(true);
            }
        });
    });
    $(document).on('keyup', function (e) {
        if (e.key === 'Escape') {
            $resetButton.click();
        }
    });

    function changeVisitSlide(val) {
        var isActiveCls = 'is-active';

        $('[data-visit-slide]').removeClass(isActiveCls);
        $('[data-visit-slide="' + val + '"]').addClass(isActiveCls);
    }

    function Parallax(options) {
        options = options || {};
        this.nameSpaces = {
            wrapper: options.wrapper || '.js-parallax',
            layers: options.layers || '.js-parallax-layer',
            deep: options.deep || 'data-parallax-deep'
        };
        this.init = function () {
            var self = this,
                parallaxWrappers = document.querySelectorAll(this.nameSpaces.wrapper);
            for (var i = 0; i < parallaxWrappers.length; i++) {
                (function (i) {
                    parallaxWrappers[i].addEventListener('mousemove', function (e) {
                        var x = e.clientX,
                            y = e.clientY,
                            layers = parallaxWrappers[i].querySelectorAll(self.nameSpaces.layers);
                        for (var j = 0; j < layers.length; j++) {
                            (function (j) {
                                var deep = layers[j].getAttribute(self.nameSpaces.deep),
                                    disallow = layers[j].getAttribute('data-parallax-disallow'),
                                    itemX = disallow && disallow === 'x' ? 0 : x / deep,
                                    itemY = disallow && disallow === 'y' ? 0 : y / deep;
                                if (disallow && disallow === 'both') return;
                                layers[j].style.transform = 'translateX(' + itemX + '%) translateY(' + itemY + '%)';
                            })(j);
                        }
                    });
                })(i);
            }
        };
        this.init();
        return this;
    }

    window.addEventListener('load', function () {
        new Parallax();
    });

    // form
    $('.form__form').append('<input id="check" name="check" type="hidden" value="" />');

    // floating labels
    $('.is-floating-label input, .is-floating-label textarea').on('focus blur', function (e) {
        $(this).parents('.is-floating-label').toggleClass('is-focused', e.type === 'focus' || this.value.length > 0);
    }).trigger('blur').first().trigger('focus');

    // check inputs
    $('.form__button').on('click', function () {
        var $form = $(this).parents('.form');
        var $input = $form.find('.form__input_name');
        var $inputEmail = $form.find('.form__input_email');
        var $inputInfo = $form.find('.form__textarea');

        if ($input.val().length === 0) {
            $input.parents('.form__field').addClass('warning');
        }

        if ($inputEmail.val().length === 0 || $inputEmail.attr('pattern') && !/^[0-9a-z-\.]+\@[0-9a-z-]{2,}\.[a-z]{2,}$/i.test($inputEmail.val())) {
            $inputEmail.parents('.form__field').addClass('warning');
        }

        if ($inputInfo.val().length === 0) {
            $inputInfo.parents('.form__field').addClass('warning');
        }

        var $warnField = $('.form__field.warning');
        $warnField.on('input', function () {
            $(this).removeClass('warning');
        });
    });

    $('._mask-phone').inputmask({
        mask: '+9 (999) 999-99-99',
        showMaskOnHover: false
    });
});
//# sourceMappingURL=main.js.map
