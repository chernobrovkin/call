<?php
spl_autoload_register(function ($class) {
    $path = preg_replace_callback(
        '/([A-Z])(.*?)\\\\/',
        function ($m) {
            return strtolower($m[1]) . $m[2]. '/';
        },
        $class
    );

    $file = $_SERVER['DOCUMENT_ROOT'] . '/lib/' . $path . '.php';

    if (file_exists($file)) {
        require_once $file;
    }
});