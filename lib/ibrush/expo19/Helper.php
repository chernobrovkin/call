<?php

namespace Ibrush\Expo19;

use Illuminate\Database\Capsule\Manager as Capsule;

class Helper
{
    public static function getCurrentPhone($format = true)
    {
        $app = Application::getInstance();

        $phone = Capsule::table('contacts')
            ->select('PHONE')
            ->where('ID', $app->getCurrentContactId())
            ->get()
            ->pop()
            ->PHONE;

        if ($format) {
            $phone = preg_replace('/^(\d)(\d{3})(\d{3})(\d{2})(\d{2})$/', '+$1 $2 $3-$4-$5', $phone);
        }

        return $phone;
    }

}