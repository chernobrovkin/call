<?php
namespace Ibrush\Expo19;

use Monolog\Logger;
use Monolog\Handler\StreamHandler;

abstract class ApiBase
{
    public $requestParams = [];
    public $requestUri;
    public $logger;

    public $action;
    public $method;

    const API_KEY = 'dlucyifrz5z49ocm6ewx0m0c19yp3k66';
    const API_SALT = 'vq9jze1p9qk84ml1q7ohkfjbe8q8im54';

    public function __construct()
    {
        header("Access-Control-Allow-Orgin: *");
        header("Access-Control-Allow-Methods: *");
        //header("Content-Type: application/json");

        $this->requestUri = $this->prepareUri($_SERVER['REQUEST_URI']);
        $this->requestParams = $_REQUEST;
        $this->logger = new Logger(
            'api',
            [new StreamHandler($_SERVER['DOCUMENT_ROOT'] . '/log/api.log')]
        );

        $this->method = $_SERVER['REQUEST_METHOD'];
        if ($this->method == 'POST' && array_key_exists('HTTP_X_HTTP_METHOD', $_SERVER)) {
            if ($_SERVER['HTTP_X_HTTP_METHOD'] == 'DELETE') {
                $this->method = 'DELETE';
            } else if ($_SERVER['HTTP_X_HTTP_METHOD'] == 'PUT') {
                $this->method = 'PUT';
            } else {
                throw new Exception("Unexpected Header");
            }
        }
    }

    public function run()
    {
        $this->logger->debug($this->requestUri);
        $this->logger->debug(print_r($this->requestParams, true));

        $this->action = $this->getAction();

        $this->checkRequest();

        if (method_exists($this, $this->action)) {
            $this->{$this->action}(json_decode($this->requestParams['json'], true));
        }
    }

    public function checkRequest()
    {
        if ($this->requestParams['vpbx_api_key'] !== self::API_KEY) {
            throw new Exception('vpbx_api_key doesn\'t match');
        }

        $sign = hash('sha256', self::API_KEY . $this->requestParams['json'] . self::API_SALT);
        if ($this->requestParams['sign'] !== $sign) {
            throw new Exception('sign doesn\'t match');
        }
    }


    private function requestStatus($code) {
        $status = array(
            200 => 'OK',
            404 => 'Not Found',
            405 => 'Method Not Allowed',
            500 => 'Internal Server Error',
        );
        return ($status[$code])?$status[$code]:$status[200];
    }


    protected function response($data, $status = 200)
    {
        header("HTTP/1.1 " . $status . " " . $this->requestStatus($status));
        return json_encode($data);
    }

    public function prepareUri($requestUri)
    {
        return preg_replace('/\/api\//', '', $requestUri);
    }

    public function getAction()
    {
        return preg_replace_callback(
            '/\/./',
            function ($m) {
                return strtoupper(substr($m[0], 1));
            },
            $this->requestUri
        );
    }

}