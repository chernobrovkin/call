<?php
namespace Ibrush\Expo19;

use Illuminate\Database\Capsule\Manager as Capsule;

class Api extends ApiBase
{
    public function eventsDtmf($data)
    {
        if ($data['to_number'] === '74955404768' && $data['dtmf'] === '1') {

            $app = Application::getInstance();

            if ($app->getState() === Application::STATE_THANKS) {
                $app->changeState();
            }

            if ($app->getState() === Application::STATE_START) {
                $id = Capsule::table('contacts')->insertGetId([
                    'PHONE' => $data['from_number'],
                    'CALL_START' => new \DateTime()
                ]);

                $app->changeState();
                $app->setCurrentContactId($id);
            }
        }
    }

    public function eventsSummary($data)
    {
        if ($data['to']['number'] !== '74955404768') {
            return '';
        }

        if ($data['from']['number'] !== Helper::getCurrentPhone(false)) {
            return '';
        }

        $app = Application::getInstance();

        if ($app->getState() === Application::STATE_RING) {
            Capsule::table('contacts')
                ->where('ID', $app->getCurrentContactId())
                ->update(['CALL_END' => new \DateTime()]);
            $app->changeState();
        }
    }
}