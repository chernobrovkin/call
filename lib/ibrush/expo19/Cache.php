<?php

namespace Ibrush\Expo19;

use Doctrine\Common\Cache\FilesystemCache;


class Cache
{
    private $driver;

    public function __construct()
    {
        $this->driver = new FilesystemCache($_SERVER['DOCUMENT_ROOT'] . '/cache/');
    }

    public function save($key, $value)
    {
        $this->driver->save($key, $value);
    }

    public function fetch($key)
    {
        return $this->driver->fetch($key);
    }
}