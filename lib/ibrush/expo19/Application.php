<?php

namespace Ibrush\Expo19;

use Illuminate\Database\Capsule\Manager as Capsule;

class Application
{
    private $state;
    private $currentContactId;
    public $cache;
    private static $instance;

    const STATE_START = 0;
    const STATE_RING = 1;
    const STATE_FORM = 2;
    const STATE_THANKS = 3;

    const STATE_PAGES = [
        'start.php',
        'ring.php',
        'form.php',
        'thanks.php'
    ];

    private function __construct()
    {
        $this->cache = new Cache();
        $this->state = $this->cache->fetch('state') ?: self::STATE_START;
        $this->currentContactId = $this->cache->fetch('current_contact_id') ?? false;

        $this->connectToDb();
    }

    public static function getInstance()
    {
        if (empty(self::$instance)) {
            self::$instance = new Application();
        }
        return self::$instance;
    }

    private function connectToDb()
    {
        $capsule = new Capsule;

        $capsule->addConnection([
            'driver'    => 'mysql',
            'host'      => 'localhost',
            'database'  => 'expo19',
            'username'  => 'ibrush',
            'password'  => 'o3nrinwovdni02',
            'charset'   => 'utf8',
            'collation' => 'utf8_unicode_ci',
            'prefix'    => '',
        ]);

        $capsule->setAsGlobal();
    }

    public function run()
    {
        if ($_SERVER['REQUEST_METHOD'] === 'GET') {
            require $_SERVER['DOCUMENT_ROOT'] . '/pages/template/header.php';
            require $this->displayPage();
            require $_SERVER['DOCUMENT_ROOT'] . '/pages/template/footer.php';
        } elseif (preg_match('/^\/api\//', $_SERVER['REQUEST_URI'])) {
            $this->processApi();
        } elseif (isset($_POST['check'])) {
            $this->processSubmit();
        } else {
            $this->processAjax();
        }
    }

    private function displayPage()
    {
        return $_SERVER['DOCUMENT_ROOT'] . '/pages/include/' . self::STATE_PAGES[$this->state];
    }

    private function processApi()
    {
        try {
            $api = new Api();
            $api->run();
        } catch (Exception $e) {
            echo json_encode(['error' => $e->getMessage()]);
        }
    }

    private function processSubmit()
    {
        $fields = $_POST;
        unset($fields['check']);
        $fields['PHONE'] = preg_replace('/[\s+\-()]/', '', $fields['PHONE']);

        Capsule::table('contacts')
            ->where('ID', $this->getCurrentContactId())
            ->update($fields);

        $this->setCurrentContactId(false);
        $this->changeState();
        header("Location: /");
    }

    private function processAjax()
    {
        echo $this->{$_POST['method']}();
    }

    public function getCurrentContactId()
    {
        return $this->currentContactId;
    }

    public function setCurrentContactId($id)
    {
        $this->currentContactId = $id;
        $this->cache->save('current_contact_id', $this->currentContactId);
    }

    public function getState()
    {
        return $this->state;
    }

    public function changeState()
    {
        $this->state = ($this->state + 1) % 4;
        $this->cache->save('state', $this->state);

        return $this->state;
    }

    public function resetState()
    {
        $this->state = self::STATE_START;
        $this->cache->save('state', $this->state);

        $this->setCurrentContactId(false);
    }
}